const programas_internacionales = (sequelize, DataTypes) => {
    const Programas = sequelize.define("programas_internacionales",{
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        telefono: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nombre_alt: {
            type: DataTypes.STRING,
            allowNull: true
        },
        email_alt: {
            type: DataTypes.STRING,
            allowNull: true
        }
    },{
        indexes:[
            {
            unique: true,
            fields: ['email', 'telefono']
            }
        ],
        schema: 'sede_info'
    });

    return Programas;
};

export default programas_internacionales;