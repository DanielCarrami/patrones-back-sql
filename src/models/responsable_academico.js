const responsable_academico = (sequelize, DataTypes) => {
    const Responsable = sequelize.define('responsable_academico',{
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
            }
        },
        email: { 
            type: DataTypes.STRING,
            allowNull: false,
            validate:{
                isEmail: true
            }
        },
        telefono: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nombre_alt: {
            type: DataTypes.STRING, 
            allowNull: true
        },
        email_alt: {
            type: DataTypes.STRING,
            allowNull: true
        },
    },{
        indexes:[
            {
            unique: true,
            fields: ['email', 'telefono']
            }
        ],
        schema: 'sede_info'
    });
  
    return Responsable; 
};
 
export default responsable_academico;  