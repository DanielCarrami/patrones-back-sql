import Sequelize from 'sequelize';
 
const sequelize_postgres = new Sequelize(
  process.env.DATABASE,
  process.env.DATABASE_USER1,
  process.env.DATABASE_PASSWORD,
  {
    dialect: 'postgres',
  }
);  

const models = {
  Teaching: sequelize_postgres.import('./teaching'),
  Contacto: sequelize_postgres.import('./contacto_captacion'),
  Programas: sequelize_postgres.import('./programas_internacionales'),
  Responsable: sequelize_postgres.import('./responsable_academico'),
  Sede: sequelize_postgres.import('./sede'),
  Tutor: sequelize_postgres.import('./tutor'),
  Asistente: sequelize_postgres.import('./asistente'),
}; 
 

Object.keys(models).forEach(key => {
  if ('associate' in models[key]) {
    models[key].associate(models);
  }
});

export { sequelize_postgres, models};
 