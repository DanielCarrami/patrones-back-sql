const contacto_captacion = (sequelize, DataTypes) => {
    const Contacto = sequelize.define("contacto_captacion",{
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        telefono: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nombre_alt: {
            type: DataTypes.STRING,
            allowNull: true
        },
        email_alt: {
            type: DataTypes.STRING,
            allowNull: true
        }
    },{
        indexes:[
            {
            unique: true,
            fields: ['email', 'telefono']
            }
        ],
        schema: 'sede_info'
    });

    return Contacto;
};

export default contacto_captacion;