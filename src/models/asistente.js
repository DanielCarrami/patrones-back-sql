
const asistente = (sequelize, DataTypes) => {
    const Asistente = sequelize.define('asistente',{
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        apellido: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        direccion: {
            type: DataTypes.STRING,
            allowNull: false
        },
        telefono: {
            type: DataTypes.STRING,
            allowNull: false
        },
        fecha_de_nacimiento: {
            type: DataTypes.DATE,
            allowNull: false
        },
        escuela: {
            type: DataTypes.STRING,
            allowNull: false
        },
        tipo_escuela: {
            type: DataTypes.ENUM(['Privada', 'Pública']),
            allowNull: false
        },
        pais: {
            type: DataTypes.STRING,
            allowNull: false
        },
        estado: {
            type: DataTypes.STRING,
            allowNull: false
        },
        municipio: {
            type: DataTypes.STRING,
            allowNull: false
        },
        ciudad: {
            type: DataTypes.STRING,
            allowNull: false
        },
        codigo_postal: {
            type: DataTypes.STRING,
            allowNull: false
        },
        grado: {
            type: DataTypes.ENUM(['Primero de preparatoria', 'Segundo de preparatoria', 'Tercero de preparatoria']),
            allowNull: false
        },
        promedio: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        participo_antes: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        clase_previa: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        estudiar_tic: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        como_entro: {
            type: DataTypes.STRING,
            allowNull: false
        },
        talla: {
            type: DataTypes.ENUM(['XS','S','M','G','XG','XXL']),
            allowNull: false
        },
        estatura: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        aviso_privacidad: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        autorizacion_medios: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        tiene_laptop: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        nombre_diploma: {
            type: DataTypes.STRING,
            allowNull: false
        },
        autorizado:{
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        indexes: [{
            name: 'inscripcion_repetida',
            unique: true,
            fields: ['nombre', 'apellido','fecha_de_nacimiento']
        }],
        schema: 'asistente_info',
    });

    Asistente.associate = models => {
        Asistente.hasOne(models.Tutor, {foreignKey: 'asistente_id', onDelete: 'CASCADE'});
    };

    return Asistente;
}; 

export default asistente;
