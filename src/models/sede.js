

const sede = (sequelize, DataTypes) =>{
    const Sede = sequelize.define('sede', {
        nombre_institucion: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        estado: {
            type: DataTypes.ENUM(['Aguascalientes','Ciudad de México','Estado de México','Jalisco','Michoacán','Nuevo León','Sinaloa','Yucatán','Chiapas','Baja California','Sonora','Guerrero','Morelos','Tamaulipas','Hidalgo','Zacatecas','Coahuila','Tabasco','Quintana Roo','Nayarit','Baja California Sur','Guanajuato','Durango','Puebla','Oaxaca','Veracrúz','Querétaro','San Luis Potosi','Tlaxcala','Campeche','Colima','Chihuahua']),
            allowNull: false
        },
        nivel: {
            type: DataTypes.ENUM(['Secundaria', 'Preparatoria', 'Universidad']),
            allowNull: false
        },
        campus: {
            type: DataTypes.ENUM(['Campus Chihuahua','Campus Ciudad de México','Campus Cuernavaca','Campus Estado de México','Campus Guadalajara','Campus León','Campus Monterrey','Campus Puebla','Campus Querétaro','Campus San Luis Potosí','Campus Santa Fé']),
            allowNull: false
        },
        direccion: {
            type: DataTypes.STRING,
            allowNull: false
        },
        participo_antes: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        labo_disp: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        no_participantes: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        salones_disp: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        no_profesores: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        exp_hospedaje: {
            type: DataTypes.STRING,
            allowNull: false
        },
        dist_sede: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        cantidad_asistentes: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        compromiso_alimentos: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        compromiso_fondos: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        compromiso_transportar: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        compromiso_permisos: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        compromiso_apoyo: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        compromiso_fotos: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        aceptar_datos: {
            type: DataTypes.BOOLEAN,
            allowNull: false 
        },
        comentarios: {
            type: DataTypes.STRING, 
            allowNull: false 
        },
        autorizado:{
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    },{ 
        schema: 'sede_info'
    });    

    Sede.associate = models => {
        Sede.hasOne(models.Responsable, {foreignKey: 'sede_id', onDelete: 'CASCADE'});
        Sede.hasOne(models.Programas, {foreignKey: 'sede_id', onDelete: 'CASCADE'});
        Sede.hasOne(models.Contacto, {foreignKey: 'sede_id', onDelete: 'CASCADE'});
        Sede.hasMany(models.Teaching, {foreignKey: 'sede_id', onDelete: 'SET NULL'});
        Sede.hasMany(models.Asistente, {foreignKey: 'sede_id', onDelete: 'SET NULL'});
    }
 
    return Sede; 
}; 
  
export default sede;  