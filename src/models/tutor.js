const tutor = (sequelize, DataTypes) => {
    const Tutor = sequelize.define("tutor",{
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate:{
                isEmail: true
            }
        },
        telefono: {
            type: DataTypes.STRING, 
            allowNull: false
        },
        nombre_alt: {
            type: DataTypes.STRING,
            allowNull: true
        }, 
        email_alt: {
            type: DataTypes.STRING,
            allowNull: true
        }
    },{
        schema: 'asistente_info',
    });

    return Tutor;
};

export default tutor;