
const teaching = (sequelize, DataTypes) => {
    const Teaching = sequelize.define('teaching',{
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        apellido: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING, 
            allowNull: false
        }, 
        telefono: {
            type: DataTypes.STRING,
            allowNull: false
        },
        edad: {
            type: DataTypes.INTEGER, 
            allowNull: false
        },
        aviso_privacidad: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        estado: {
            type: DataTypes.STRING,
            allowNull: false
        },
        ciudad: {
            type: DataTypes.STRING,
            allowNull: false
        },
        carrera: {
            type: DataTypes.STRING,
            allowNull: false
        },
        anio_graduacion: {
            type: DataTypes.STRING,
            allowNull: false
        },
        universidad: {
            type: DataTypes.STRING,
            allowNull: false
        },
        disp_cambio_sede: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        semestre_carrera: {
            type: DataTypes.ENUM(['Graduado','Primero','Segundo','Tercero','Cuarto','Quinto','Sexto','Septimo','Octavo','Noveno','Decimo','Onceavo']),
            allowNull: false
        }, 
        razon_participacion: {
            type: DataTypes.STRING,
            allowNull: false
        },
        experiencia_previa: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        razon_carrera: {
            type: DataTypes.STRING,
            allowNull: false
        },
        disponibilidad_hospedaje: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        autorizado:{
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    },{
        indexes:[
            {
            unique: true,
            fields: ['email', 'telefono']
            }
        ]
    });

    Teaching.findByName = async (name) => {
        let teaching = await Teaching.findOne({
            where: { nombre: name },
        }); 
        return teaching; 
    };
    return Teaching;
};

export default teaching;