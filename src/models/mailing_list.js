/*
MYSQL MODEL
*/

const mailing_list = (sequelize, DataTypes) => {
    const Mailing_list = sequelize.define("Mailing_list",{
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        politicas_privacidad: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    });

    return Mailing_list;
};

export default mailing_list;