import { Router } from 'express';


const router = Router();

router.get('/', async(req, res) => {
    const tutor = await req.context.models.Tutor.findAll();
    return res.send(tutor);
});

router.get('/:tutorId', async (req, res) => {
    const tutor = await req.context.models.Tutor.findByPk(
        req.params.tutorId,
    ); 
    return res.send(tutor);
});

router.post('/', async (req, res) => {
    const tutor = await req.context.models.Tutor.create({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt,
        email_alt: req.body.email_alt,
        asistente_id: req.body.asistente_id
    });

    return res.send(tutor); 
});

router.put('/update/:tutorId', async (req, res) => {
    const tutor = await req.context.models.Tutor.findByPk(
        req.params.tutorId,
    ); 

    if(!tutor){
        return res.status(400).send({
            status: 400, 
            message: 'No hay tutor con ese id'
        });
    }

    const newTutor = await req.context.models.Tutor.update({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt, 
        email_alt: req.body.email_alt,
        asistente_id: req.body.asistente_id
    }, {where: {id: req.params.tutorId}})
    .then(updatedParticipant => res.status(200).send(updatedParticipant))
    .catch(error => {res.status(400).send(error)}); 

    return newTutor;
});

router.delete('/:tutorId', async(req, res) => {
    const tutor = await req.context.models.Tutor.destroy({
        where: {id: req.params.tutorId }, 
    });

    return res.send("Tutor eliminado");
});

export default router;