import { Router } from 'express';

const router = Router();

router.get('/', async(req, res) => {
    const contacto = await req.context.models.Contacto.findAll();
    return res.send(contacto);
});

router.get('/:contactoId', async (req, res) => {
    const contacto = await req.context.models.Contacto.findByPk(
        req.params.contactoId,
    );
    return res.send(contacto);
});

router.put('/update/:contactoId', async (req, res) => {
    const contacto = await req.context.models.Contacto.findByPk(
        req.params.contactoId,
    ); 

    if(!contacto){
        return res.status(400).send({
            status: 400, 
            message: 'No hay contacto de captación con ese id'
        });
    }

    const newContacto = await req.context.models.Contacto.update({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt, 
        email_alt: req.body.email_alt,
        asistente_id: req.body.asistente_id
    }, {where: {id: req.params.contactoId}})
    .then(updated => res.status(200).send(updated))
    .catch(error => {res.status(400).send(error)}); 

    return newContacto;
});

router.post('/', async (req, res) => {
    const contacto = await req.context.models.Contacto.create({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt,
        email_alt: req.body.email_alt,
        sede_id: req.body.sede_id
    });

    return res.send(contacto);
});

router.delete('/:contactoId', async(req, res) => {
    const contacto = await req.context.models.Contacto.destroy({
        where: {id: req.params.contactoId },
    });

    return res.send("Contacto eliminado");
});

export default router;