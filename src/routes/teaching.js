import { Router } from 'express';
import { sequelize_postgres } from '../models/index';

const { QueryTypes } = require('sequelize');
const router = Router();

router.get('/', async (req, res) => {
    const teaching = await req.context.models.Teaching.findAll();
    return res.send(teaching);
});

router.get('/:teachingId', async (req, res) => {
    const teaching = await req.context.models.Teaching.findByPk(
        req.params.teachingId,
    );
    return res.send(teaching);
}); 

router.put('/update/:teachingId', async (req, res) => {
    const teaching = await req.context.models.Teaching.findByPk(
        req.params.teachingId,
    ); 

    if(!teaching){
        return res.status(400).send({
            status: 400, 
            message: 'No hay asesor con ese id'
        });
    }

    const newTeaching = await req.context.models.Teaching.update({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        email: req.body.email,
        telefono: req.body.telefono,
        edad: req.body.edad,
        aviso_privacidad: req.body.aviso_privacidad,
        estado: req.body.estado,
        ciudad: req.body.ciudad,
        carrera: req.body.carrera,
        anio_graduacion: req.body.anio_graduacion,
        universidad: req.body.universidad,
        disp_cambio_sede: req.body.disp_cambio_sede,
        semestre_carrera: req.body.semestre_carrera,
        razon_participacion: req.body.razon_participacion,
        experiencia_previa: req.body.experiencia_previa,
        razon_carrera: req.body.razon_carrera,
        disponibilidad_hospedaje: req.body.disponibilidad_hospedaje,
        sede_id: req.body.sede_id,
        autorizado: req.body.autorizado
    }, {where: {id: req.params.teachingId}})
    .then(updatedTeaching => res.status(200).send(updatedTeaching))
    .catch(error => {res.status(400).send(error)}); 

    return newTeaching;
});

router.post('/', async (req, res) => {
    const t = await sequelize_postgres.transaction();
    try{ 
    const teaching = await req.context.models.Teaching.create({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        email: req.body.email,
        telefono: req.body.telefono,
        edad: req.body.edad,
        aviso_privacidad: req.body.aviso_privacidad,
        estado: req.body.estado,
        ciudad: req.body.ciudad,
        carrera: req.body.carrera,
        anio_graduacion: req.body.anio_graduacion,
        universidad: req.body.universidad,
        disp_cambio_sede: req.body.disp_cambio_sede,
        semestre_carrera: req.body.semestre_carrera,
        razon_participacion: req.body.razon_participacion,
        experiencia_previa: req.body.experiencia_previa,
        razon_carrera: req.body.razon_carrera,
        disponibilidad_hospedaje: req.body.disponibilidad_hospedaje,
        sede_id: req.body.sede_id,
        autorizado: false
    }, {transaction: t});  
        
    await t.commit();

    return res.send(teaching); 

    } catch(e){ 
        console.log('Y con ustedes, el error \n' + e);
        await t.rollback();
        return res.status(404).send({
            status: 400,
            message: 'La transacción no fue exitosa',
        });
    } 
        
});

router.post('/apoyo_sede/:sedeId', async (req, res) => {
    const q = await sequelize_postgres.query(
        'CALL change_teaching_to_sede(?);',
        {
            replacements: [req.params.sedeId],
            type: QueryTypes.RAW
        });

    return res.send(q);
});

router.delete('/:teachingId', async (req, res) => {
    const teaching = await req.context.models.Teaching.destroy({
        where: { id: req.params.teachingId },
    });

    return res.send("Asistente eliminado");
});

export default router;