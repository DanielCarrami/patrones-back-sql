import { Router } from 'express';
import {sequelize_postgres} from '../models/index';
import {QueryTypes} from 'sequelize';

const router = Router();

router.get('/', async (req, res) => {
    const sede = await req.context.models.Sede.findAll({
        include: [{
            model: req.context.models.Responsable
        },
        {
            model: req.context.models.Programas
        },
        {
            model: req.context.models.Contacto
        },
        {
            model: req.context.models.Teaching
        },
        {
            model: req.context.models.Asistente
        }
    ]
    });
    return res.send(sede);
}); 

router.get('/id_name', async (req, res) => {
 const sede = await req.context.models.Sede.findAll({
        attributes:['id','nombre_institucion'],
        where:[{'autorizado': true}]
    });
    return res.send(sede);
});

router.get('/:sedeId', async (req, res) => { 
    const sede = await req.context.models.Sede.findByPk( 
        req.params.sedeId, 
    );  
    return res.send(sede);   
});

router.put('/update/:sedeId', async (req, res) => {
    const sede = await req.context.models.Sede.findByPk(
        req.params.sedeId,
    ); 

    if(!sede){
        return res.status(400).send({
            status: 400, 
            message: 'No hay sede con ese id'
        });
    }

    const newSede = await req.context.models.Sede.update({
        nombre_institucion: req.body.nombre_institucion,
        estado: req.body.estado,
        nivel: req.body.nivel, 
        campus: req.body.campus,
        direccion: req.body.direccion,
        participo_antes: req.body.participo_antes,
        labo_disp: req.body.labo_disp,
        no_participantes: req.body.no_participantes,
        salones_disp: req.body.salones_disp,
        no_profesores: req.body.no_profesores,
        exp_hospedaje: req.body.exp_hospedaje,
        dist_sede: req.body.dist_sede,
        cantidad_asistentes: req.body.cantidad_asistentes,
        compromiso_alimentos: req.body.compromiso_alimentos,
        compromiso_fondos: req.body.compromiso_fondos,
        compromiso_transportar: req.body.compromiso_transportar,
        compromiso_permisos: req.body.compromiso_permisos,
        compromiso_apoyo: req.body.compromiso_apoyo,
        compromiso_fotos: req.body.compromiso_fotos,
        aceptar_datos: req.body.aceptar_datos,
        comentarios: req.body.comentarios,
        autorizado: req.body.autorizado
    }, {where: {id: req.params.sedeId}})
    .then(updatedCampus => res.status(200).send(updatedCampus))
    .catch(error => {res.status(400).send(error)}); 

    return newSede;
});
 
router.post('/', async (req, res) => {  
    const t = await sequelize_postgres.transaction();
    try{
    const sede = await req.context.models.Sede.create({
        nombre_institucion: req.body.nombre_institucion,
        estado: req.body.estado,
        nivel: req.body.nivel, 
        campus: req.body.campus,
        direccion: req.body.direccion,
        participo_antes: req.body.participo_antes,
        labo_disp: req.body.labo_disp,
        no_participantes: req.body.no_participantes,
        salones_disp: req.body.salones_disp,
        no_profesores: req.body.no_profesores,
        exp_hospedaje: req.body.exp_hospedaje,
        dist_sede: req.body.dist_sede,
        cantidad_asistentes: req.body.cantidad_asistentes,
        compromiso_alimentos: req.body.compromiso_alimentos,
        compromiso_fondos: req.body.compromiso_fondos,
        compromiso_transportar: req.body.compromiso_transportar,
        compromiso_permisos: req.body.compromiso_permisos,
        compromiso_apoyo: req.body.compromiso_apoyo,
        compromiso_fotos: req.body.compromiso_fotos,
        aceptar_datos: req.body.aceptar_datos,
        comentarios: req.body.comentarios,
        autorizado: false
    }, {transaction: t});

    await t.commit();
    return res.send(sede);
    }catch(e){
        console.log('TRANSACCION INCOMPLETA \n' + e);
        await t.rollback();
        return res.status(404).send({
            status: 400,
            message: 'La transacción no fue exitosa',
        });
    }
});

router.delete('/:sedeId', async (req, res) => {
    const sede = await req.context.models.Sede.destroy({
        where: { id: req.params.sedeId },
    });

    return res.send("Sede eliminada");
});


export default router;