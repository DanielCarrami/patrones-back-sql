import { Router } from 'express';

const router = Router();

router.get('/', async(req, res) => {
    const responsable = await req.context.models.Responsable.findAll();
    return res.send(responsable);
});

router.get('/:responsableId', async (req, res) => {
    const responsable = await req.context.models.Responsable.findByPk(
        req.params.responsableId,
    );
    return res.send(responsable);
});

router.put('/update/:responsableId', async (req, res) => {
    const responsable = await req.context.models.Responsable.findByPk(
        req.params.responsableId,
    ); 

    if(!responsable){
        return res.status(400).send({
            status: 400, 
            message: 'No hay responsable acedemico con ese id'
        });
    }

    const newResponsable = await req.context.models.Responsable.update({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt, 
        email_alt: req.body.email_alt,
        asistente_id: req.body.asistente_id
    }, {where: {id: req.params.responsableId}})
    .then(updated => res.status(200).send(updated))
    .catch(error => {res.status(400).send(error)}); 

    return newResponsable;
});

router.post('/', async (req, res) => {
    const responsable = await req.context.models.Responsable.create({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt,
        email_alt: req.body.email_alt,
        sede_id: req.body.sede_id
    });

    return res.send(responsable);
});

router.delete('/:responsableId', async(req, res) => {
    const responsable = await req.context.models.Responsable.destroy({
        where: {id: req.params.responsableId },
    });

    return res.send("Responsable eliminado");
});

export default router;