import teaching from './teaching';
import contacto from './contacto_captaciones';
import programas from './programas_internacionales';
import responsable from './responsable_academico';
import tutor from './tutor';
import sede from './sede';
import asistente from './asistente';

export default {
    teaching,
    contacto,
    programas,
    responsable,
    sede,
    tutor,
    asistente
};