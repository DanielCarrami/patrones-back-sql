import { Router } from 'express';
import { sequelize_postgres } from '../models/index';

const router = Router();

router.get('/', async (req, res) => {
    const asistente = await req.context.models.Asistente.findAll({
        include: [{
            model: req.context.models.Tutor
        }]
    });

    return res.send(asistente);
});

router.get('/:asistenteId', async (req, res) => {
    const asistente = await req.context.models.Asistente.findByPk(
        req.params.asistenteId,
    );
    return res.send(asistente); 
});

router.put('/update/:asistenteId', async (req, res) => {
    const asistente = await req.context.models.Asistente.findByPk(
        req.params.asistenteId,
    ); 

    if(!asistente){
        return res.status(400).send({
            status: 400, 
            message: 'No hay asistente internacionales con ese id'
        });
    }

    const newAsistente = await req.context.models.Asistente.update({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        email: req.body.email,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        fecha_de_nacimiento: req.body.fecha_de_nacimiento,
        escuela: req.body.escuela,
        tipo_escuela: req.body.tipo_escuela,
        pais: req.body.pais,
        estado: req.body.estado,
        municipio: req.body.municipio,
        ciudad: req.body.ciudad,
        codigo_postal: req.body.codigo_postal,
        grado: req.body.grado,
        promedio: req.body.promedio,
        participo_antes: req.body.participo_antes,
        clase_previa: req.body.clase_previa, 
        estudiar_tic: req.body.estudiar_tic,
        como_entro: req.body.como_entro,
        talla: req.body.talla,
        estatura: req.body.estatura,
        aviso_privacidad: req.body.aviso_privacidad, 
        autorizacion_medios: req.body.autorizacion_medios,
        tiene_laptop: req.body.tiene_laptop,
        nombre_diploma: req.body.nombre_diploma,
        sede_id: req.body.sede_id,
        autorizado: req.body.autorizado
    }, {where: {id: req.params.asistenteId}})
    .then(updated => res.status(200).send(updated))
    .catch(error => {res.status(400).send(error)}); 

    return newAsistente;
});
 
router.post('/', async (req, res) => {
    const t = await sequelize_postgres.transaction();
    try{ 
        const asistente = await req.context.models.Asistente.create({
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            email: req.body.email,
            direccion: req.body.direccion,
            telefono: req.body.telefono,
            fecha_de_nacimiento: req.body.fecha_de_nacimiento,
            escuela: req.body.escuela,
            tipo_escuela: req.body.tipo_escuela,
            pais: req.body.pais,
            estado: req.body.estado,
            municipio: req.body.municipio,
            ciudad: req.body.ciudad,
            codigo_postal: req.body.codigo_postal,
            grado: req.body.grado,
            promedio: req.body.promedio,
            participo_antes: req.body.participo_antes,
            clase_previa: req.body.clase_previa, 
            estudiar_tic: req.body.estudiar_tic,
            como_entro: req.body.como_entro,
            talla: req.body.talla,
            estatura: req.body.estatura,
            aviso_privacidad: req.body.aviso_privacidad, 
            autorizacion_medios: req.body.autorizacion_medios,
            tiene_laptop: req.body.tiene_laptop,
            nombre_diploma: req.body.nombre_diploma,
            sede_id: req.body.sede_id,
            autorizado: false
        }, {transaction: t});  
        
        await t.commit();

        return res.send(asistente); 
 
    } catch(e){ 
        console.log('Y con ustedes, el error \n' + e);
        await t.rollback();
        if(e.parent.constraint){
            e = e.parent.constraint;
        }
        return res.status(404).send({
            status: 400,
            message: 'La transacción no fue exitosa',
            error: e
        });
    }  
});

router.delete('/:asistenteId', async(req, res) =>{
    const asistente = await req.context.models.Asistente.destroy({
        where: {id: req.params.asistenteId},
    });

    return res.send('Asistente Eliminado');
});

export default router;