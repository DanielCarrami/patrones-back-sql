import { Router } from 'express';

const router = Router();

router.get('/', async(req, res) => {
    const programas = await req.context.models.Programas.findAll();
    return res.send(programas);
});

router.get('/:programasId', async (req, res) => {
    const programas = await req.context.models.Programas.findByPk(
        req.params.programasId,
    );
    return res.send(programas);
});

router.put('/update/:programasId', async (req, res) => {
    const programas = await req.context.models.Programas.findByPk(
        req.params.programasId,
    ); 

    if(!programas){
        return res.status(400).send({
            status: 400, 
            message: 'No hay programas internacionales con ese id'
        });
    }

    const newProgramas = await req.context.models.Programas.update({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt, 
        email_alt: req.body.email_alt,
        asistente_id: req.body.asistente_id
    }, {where: {id: req.params.programasId}})
    .then(updated => res.status(200).send(updated))
    .catch(error => {res.status(400).send(error)}); 

    return newProgramas;
});

router.post('/', async (req, res) => {
    const programas = await req.context.models.Programas.create({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        nombre_alt: req.body.nombre_alt,
        email_alt: req.body.email_alt,
        sede_id: req.body.sede_id
    });

    return res.send(programas);
});

router.delete('/:programasId', async(req, res) => {
    const programas = await req.context.models.Programas.destroy({
        where: {id: req.params.programasId },
    });

    return res.send("Programa eliminado");
});

export default router;