import 'dotenv/config';
const cors = require('cors');
const express = require('express');
const compression = require('compression');
import helmet from 'helmet';

import { models, sequelize_postgres} from './models';
import routes from './routes';

const app = express();

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(async (req, res, next) => {
  req.context = {
    models
  };
  next();
}); 

/*
SEQUELIZE
*/

app.use('/teaching', routes.teaching);
app.use('/contacto_captacion', routes.contacto);
app.use('/programas_internacionales', routes.programas);
app.use('/responsable_academico', routes.responsable);
app.use('/sede', routes.sede);
app.use('/tutor', routes.tutor);
app.use('/asistente', routes.asistente);
 
//const eraseDatabaseOnSync = true;

sequelize_postgres.sync(/*{ force: eraseDatabaseOnSync }*/).then(() => {
    /*if (eraseDatabaseOnSync) {
        createTeachingDummy();
    }  */

    app.listen(process.env.PORT, () => {
      console.log(`Example app listening on port ${process.env.PORT}!`)
    });

});


const createTeachingDummy = async () => {
    await models.Sede.create({
      nombre_institucion: "Tecnológico De Monterrey",
        estado: "Puebla",
        nivel: "Universidad", 
        campus: "Campus Puebla",
        direccion: "Atlixcáyotl 5718, Reserva Territorial Atlixcáyotl, 72453 Puebla, Pue.",
        participo_antes: true,
        labo_disp: true,
        no_participantes: 50,
        salones_disp: true,
        no_profesores: 5,
        exp_hospedaje: "Se podrán hospedar en las residencias de la universidad",
        dist_sede: "5",
        cantidad_asistentes: 100,
        compromiso_alimentos: true,
        compromiso_fondos: true,
        compromiso_transportar: true,
        compromiso_permisos: true,
        compromiso_apoyo: true,
        compromiso_fotos: true,
        aceptar_datos: true,
        comentarios:"La universidad está lista para recibir el evento",
        autorizado: false
    });
    await models.Responsable.create(
      {
        nombre: "Claudia Pérez",
        email: "Clau@email.com",
        telefono: "222 303 2000",
        sede_id: 1
      }
    );
    await models.Programas.create(
      {
        nombre: "Claudia Pérez",
        email: "Clau@email.com",
        telefono: "222 303 2000",
        sede_id: 1
      }
    );
    await models.Contacto.create(
      {
        nombre: "Claudia Pérez",
        email: "Clau@email.com",
        telefono: "222 303 2000",
        sede_id: 1
      }
    );
    await models.Teaching.create(
        {
            nombre: 'Daniela',
            apellido: 'Rohana Medina',
            email: 'rohana@email.com',
            telefono: '2223612091',
            edad: 21,
            aviso_privacidad: true,
            estado: 'Puebla',
            ciudad: 'Puebla',
            carrera: 'ITC',
            anio_graduacion: '2019', 
            universidad: 'Tec de Monterrey',
            disp_cambio_sede: true,
            semestre_carrera: 'Primero',
            razon_participacion: 'Quiero ensañar y aprender',
            experiencia_previa: true,
            razon_carrera: 'Me gusta los juegos',
            disponibilidad_hospedaje: true,
            sede_id: 1,
            autorizado: false
        },
    );
    await models.Asistente.create(
      {
        nombre: 'Abigail',
        apellido: 'Cabrera',
            email: 'abi@email.com',
            direccion: 'Avenida del carmen 14',
            telefono: '2223612091',
            fecha_de_nacimiento: '2020-01-02',
            escuela: 'Colegio Humboldt',
            tipo_escuela: 'Privada',
            pais: 'México',
            estado: 'Puebla',
            municipio: 'Puebla',
            ciudad: 'Puebla',
            codigo_postal: '72154',
            grado: 'Primero',
            promedio: 99.6,
            participo_antes: true,
            clase_previa: false, 
            estudiar_tic: false,
            como_entro: true,
            talla: 'XS',
            estatura: 1.4,
            aviso_privacidad: true, 
            autorizacion_medios: true,
            tiene_laptop: true,
            nombre_diploma: 'Abigail González Cabrera',
            sede_id: 1,
            autorizado: false
      }
    );

    await models.Tutor.create(
      {
        nombre: "Claudia Pérez",
        email: "Clau@email.com",
        telefono: "222 303 2000",
        asistente_id: 1
      }
    );
} 


